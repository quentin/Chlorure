# Chlorure

*Under heavy development*

For now, chlorure uses the old way to manage go dependencies (ie: not go mod).
Given you are new to go and not set any environment variable, the following should work for you:

```
GOPATH=$HOME/go
GOBIN=$GOPATH/bin 

go get ./...
```

Then

```
go run ./cli.go
```


## Scratchpad

  - https://golang.org/pkg/crypto/cipher/ --> c'est pas clé en main, c'est soit streaming soit authentication
  - https://www.imperialviolet.org/2014/06/27/streamingencryption.html --> gpg ne fait pas l'authentication correctement
  - https://github.com/FiloSottile/age --> age fait de l'authentication et du streaming
  - https://rclone.org/crypt/ --> rclone fait de l'auth+streaming de la même manière que age mais avec un format de fichier différent (stockage du nonce, infos sur les algos utilisés)
  - https://neilmadden.blog/2019/12/30/a-few-comments-on-age/ --> une critique plutôt négative de age qui ne me donne pas envie de l'utiliser, pas plus que rclone du coup
  - https://moxie.org/2011/12/13/the-cryptographic-doom-principle.html
    --> cité par l'article précédent, je ne comprends pas trop mais je crois que pas simple
  - https://godoc.org/golang.org/x/crypto/nacl/box --> du coup je pense me limiter à un lib très reconnue comme nacl/sodium, si possible une implem officielle. Mais là pas de streaming, à nous de chunker et de gérer la rotation des nonces
  - Est ce qu'on a besoin d'authentication ?
  - Oui en fait il y a plein d'attaques apparemment
  - https://blog.minio.io/data-at-rest-encryption-done-right-7446c644ddb6 --> Minio a sa solution mais elle a des requirements bizarres (une clé par fichier, il faut donc un HKDF)
  - https://www.imperialviolet.org/2017/05/14/aesgcmsiv.html --> AES GCM SIV does not break crypto if you reuse nonces (but you should still try to supply unique ones to have different cipher if you encode the same plaintext twice)
