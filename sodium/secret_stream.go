package sodium

/*
#cgo CFLAGS: -g -Wall
#cgo LDFLAGS: -lsodium
#include <sodium.h>
*/
import "C"
import "unsafe"

const KeyLen int = C.crypto_secretstream_xchacha20poly1305_KEYBYTES
const BlockSize int = 16 * 1024 // 16 KiB

func GenerateKey() [KeyLen]byte {
  var key [KeyLen]byte
  keywrap := (*C.uchar)(unsafe.Pointer(&key[0]))
  C.crypto_secretstream_xchacha20poly1305_keygen(keywrap)
  return key
}
/*
// Maybe use io.Pipe() for io.Writer

type SecretStreamEncode struct {
  src io.Reader
  state C.crypto_secretstream_xchacha20poly1305_state
}

type SecretStreamDecode struct {
  src io.Reader
  state C.crypto_secretstream_xchacha20poly1305_state
}

func NewSecretStreamEncode(key []byte, src io.Reader) *SecretStream {
  // it's ok to call init multiple times according to sodium doc
  ret := C.sodium_init()
  if ret < 0 { log.Panic("Failed to init sodium.") }

  s := { src: src}
  var header [C.crypto_secretstream_xchacha20poly1305_HEADERBYTES]C.uchar
  C.crypto_secretstream_xchacha20poly1305_init_push(&s.state, &header[0], &key[0])
  // @FIXME do something with header

  return &s
}

// Encode
func (s *SecretStream) Read(p []byte) (n int, err error) {
  s/
}
*/
/*
func main() {

  log.Print("key", key)
  log.Print("header", header)

  var plain [block_size]C.uchar
  var c1 [block_size + C.crypto_secretstream_xchacha20poly1305_ABYTES]C.uchar

  C.crypto_secretstream_xchacha20poly1305_push(&state, &c1[0], nil, &plain[0], C.ulonglong(len(plain)), nil, 0, 0)
  log.Print("c1", c1)
}
*/
