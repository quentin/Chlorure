package sodium

import "testing"
import "log"

func TestGenerateKey(t *testing.T) {
  k := GenerateKey()
  if len(k) != KeyLen {
    t.Errorf("Wrong keylen %d instead of %d", len(k), KeyLen)
  }
  log.Printf("Generated Key: %v", k)
}
